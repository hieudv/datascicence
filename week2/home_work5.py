#Write a function that accept a string as a single argument
# and print out whether that string is a palindrome.
# (A palindrome is a string that reads the same forwards and backwards.)
# For example, "abcddcba" is a palindrome, "1221" is also a palindrome.
def is_palindrome(s):
    l=len(s)
    for i in range(l):
        if(s[i]!=s[l-i-1]): return "Not palindrome"
    return "Is palindrome"
def main():
    print(is_palindrome('abcddcba'))
if __name__ == '__main__':
    main()