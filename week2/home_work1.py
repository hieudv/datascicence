#Write code to print out 1 to 100.
# If the number is a multiple of 3, print out "fizz" instead of the number.
# If the number is a multiple of 5, print out "buzz".
# If the number is multiple of 3 and 5, print out "fizzbuzz".

def main():
    for i in range(1,101):
        if(i%3==0 and i%5==0):
            print("fizzbuzz")
        elif(i%3==0):
            print("fizz")
        elif(i%5==0):
            print("buzz")
        else:
            print(i)

if __name__ == '__main__':
    main()