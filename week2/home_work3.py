#Given a dictionary my_dict = {'a': 9, 'b': 1, 'c': 12, 'd': 7}.
# Write code to print out a list of sorted key based on their value.
# For example, in this case, the code should print out ['b', 'd', 'a', 'c']
def main():
    my_dict = {'a': 9, 'b': 1, 'c': 12, 'd': 7}
    l=(list)(my_dict.keys())
    l.sort(key=lambda x:my_dict[x])
    print(l)
if __name__ == '__main__':
    main()