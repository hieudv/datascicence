#Let’s say I give you a list saved in a variable: a = [[1, 4], [9, 16], [25, 36]].
# Write one line of Python that takes this list a and
#  makes a new list that contains [1, 4, 9, 16, 25, 36]
def main():
    a = [[1, 4], [9, 16], [25, 36]]
    b=[i for arr in a for i in arr]
    print(b)
if __name__ == '__main__':
    main()