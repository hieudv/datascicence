#Take two lists, say for example these two:
#a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
#b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
#and write a program that returns a list that contains
# only the elements that are common between the lists (without duplicates).
# Make sure your program works on two lists of different sizes.
def merge_aray(a,b):
    a=(list(set(a)))
    b=(list(set(b)))
    arr_merge=a+[i for i in b if i not in a]
    arr_merge.sort()
    return arr_merge
def main():
    a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    print(merge_aray(a,b))
if __name__ == '__main__':
    main()