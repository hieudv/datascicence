#You are given a string and width W. Write code to wrap the string into a paragraph of width W
def wrap_text(s, with_text):
    new_string=""
    while(len(s)>with_text):
        line_break=with_text-1
        while not s[line_break].isspace():
            line_break=line_break-1
        new_string=new_string+s[0:line_break]+'\n'
        s=s[line_break+1:]
    return new_string+s
def main():
    print(wrap_text("a bc de f",3))

if __name__ == '__main__':
    main()