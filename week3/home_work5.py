#Create a 3x3 matrix with values ranging from 0 to 8
import numpy as np

def main():
    matrix=np.arange(0,9).reshape(3,3)
    matrix2=np.random.randint(0,9, size=9).reshape(3,3)
    print(matrix2)

if __name__ == '__main__':
    main()