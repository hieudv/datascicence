#Find indices of non-zero elements from the array [1,2,0,0,4,0]
import numpy as np

def main():
    array=np.array([1,2,0,0,4,0])
    print(np.where(array!=0))
if __name__ == '__main__':
    main()