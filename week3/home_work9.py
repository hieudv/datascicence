#Create a 2d array with 1 on the border and 0 inside
import numpy as np
def main():
    row=4
    colum=4
    matrix=np.ones([row,colum])
    matrix[1:row-1,1:colum-1]=0
    print(matrix)
if __name__ == '__main__':
    main()