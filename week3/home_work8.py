#Create a random vector of size 30 and find the mean value
import numpy as np
def main():
    vector=np.random.random_sample((30,))
    print(vector.mean(axis=0))
    print(vector.sum(axis=0))
    print(vector)

if __name__ == '__main__':
    main()