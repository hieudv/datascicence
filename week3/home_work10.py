#Given an array x of 20 integers in the range (0, 100)
#and an random float in the range (0, 20)
#Find the index of x where the value at that index is closest to y.
import numpy as np

def main():
    x = np.random.randint(0, 100, 20)
    print(x)
    y = np.random.uniform(0, 20)
    print(y)
    #print(np.sort(np.abs(x-y)))
    print(np.argsort(np.abs(x-y))[0])

if __name__ == '__main__':
    main()